
//	Get the formatting & dom services
var formatter = prism.$injector.get('$filter'),
	$dom = prism.$injector.get('ux-controls.services.$dom');

//	Function to get a sparkline column's formatting mask
function getFormatMask(widget, idx){

	//	Look for a matching field's id
	var fields = widget.queryResult.fields.filter(function(field){ 
		return field.index === idx; 
	})
	var fieldId = $$get(fields, '0.id', '');

	//	Lookup that field's formatting mask
	var masks = widget.metadata.panel('values').items.filter(function(item){ 
		return item.field.id === fieldId; 
	})

	//	Return the mask, or an empty object
	return $$get(masks, '0.format.mask', {});
}

//	Function render a column's worth of highcharts
function createSparklines(widget, idx, config, data){

	//	Get the html elements for the widget
	var widgetContainer = (prism.$ngscope.appstate == 'widget') ? $('.pivot-body') : $('widget[widgetid="' + widget.oid + '"]'),
		tableElement = $('#pivot_', widgetContainer),
		cells = $('td.p-value[fidx=' + idx + ']', tableElement);

	//	Get the mask used for formatting the Y Axis
	var formatMask = getFormatMask(widget, idx);

	//	Calculate an offset, based on the current page and number of items per page
	var offset = parseInt(widget.style.pageSize) * widget.currentPage;

	//	Loop through each cell in the row
	cells.each(function(index, element){

		//	Define the array of data
		var cellData = data[offset + index];

		//	Tell the configuration to render to this cell
		config.chart.renderTo = element;

		//	Figure out the width of the cell (-10 for padding)
		var width = $(element).width() - 10;
		config.chart.width = width;

		//	Add the data series
		config.series = [{
            data: cellData,
            dataMask: formatMask,
			pointStart: 1
        }]

		//	Create the new highchart
		new Highcharts.Chart(config);
	})
}

/*
//	Default tooltip
function showTooltip(event){

	//	Format the value, using the mask saves at the series level
	var valueText = formatter('numeric')(this.y, this.series.options.dataMask);

	//	Check for the dimension's level (only for dates)
	var dimension = this.options.dimension;
	var dateLevel = $$get(this.options, 'dimensionLevel', null);
	var dateMask = getDateMask(dateLevel);
	
	//	Calculate the text of the dimension
	var dimensionText = dateLevel ? formatter('date')(dimension, dateMask) : dimension;

	//	Figure out the row members for this intersection
	var members = this.options.rowMembers.join(', ');

	//	Format the 
	var html = '<div style="">' 
				+ members + '<br />' 
				+ dimensionText + ': ' + valueText
			+ '</div>';
}

*/

//	Define the defaults for a sparkline chart
var defaultConfig = {
	chart: {
		renderTo: null,
		backgroundColor: null,
    	borderWidth: 0,
		type: 'line',
		margin: [2, 0, 2, 0],
        width: 120,
        height: 20,
        skipClone: true,
        style: {
            overflow: 'visible'
        }
	},
	title: {
        text: ''
    },
    credits: {
        enabled: false
    },
    exporting: {
    	enabled: false
    },
    xAxis: {
        labels: {
            enabled: false
        },
        title: {
            text: null
        },
        startOnTick: false,
        endOnTick: false,
        tickPositions: []
    },
    yAxis: {
        endOnTick: false,
        startOnTick: false,
        labels: {
            enabled: false
        },
        title: {
            text: null
        },
        tickPositions: [0]
    },
    legend: {
        enabled: false
    },
    tooltip: {
        enabled: true,
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> {point.dimension}: <b>{point.value}</b><br/>',
    },
    plotOptions: {
        series: {}
    }
};

//	Define the list of chart options for the sparkline
var chartTypes = [
	{
		label:'Line',
		tooltip: 'Line Chart',
		run: function(widget, index, data, formatting) {

			//	Start from the default settings defined above
			var config = $$.object.clone(defaultConfig, true);

			// Define the highcharts settings specific for this chart type
			config.chart.type = 'line';

			//	Define the styling options
			config.plotOptions.series = {
				animation: false,
	            dataLabels: {
	            	enabled: false
	            },
	            lineWidth: 1,
	            color: formatting.color.default,
	            shadow: false,
	            states: {
	                hover: {
	                    enabled: false
	                }
	            },
	            marker: {
	                radius: 2,
	                states: {
	                    hover: {
	                        enabled: false
	                    }
	                }
	            },
	            fillOpacity: 0.25
	        }

			//	Render the highchart
			createSparklines(widget, index, config, data)
		}
	},{
		label:'Area',
		tooltip: 'Area Chart',
		run: function(widget, index, data, formatting) {

			//	Start from the default settings defined above
			var config = $$.object.clone(defaultConfig, true);

			// Define the highcharts settings specific for this chart type
			config.chart.type = 'area';

			//	Define the styling options
			config.plotOptions.series = {
				color: formatting.color.default,
				dataLabels: {
	            	enabled: false
	            },
	            states: {
	                hover: {
	                    enabled: false
	                }
	            },
	            marker: {
	                radius: 2,
	                states: {
	                    hover: {
	                        enabled: false
	                    }
	                }
	            },
	        }

			//	Render the highchart
			createSparklines(widget, index, config, data)
		}
	},{
		label:'Column',
		tooltip: 'Column Chart',
		run: function(widget, index, data, formatting) {

			//	Start from the default settings defined above
			var config = $$.object.clone(defaultConfig, true);

			// Define the highcharts settings specific for this chart type
			config.chart.type = 'column';

			//	Define the styling options
			config.plotOptions.series = {
				dataLabels: {
	            	enabled: false
	            }
	        }

	        //	Hide the x axis
	        config.xAxis.visible = false;

			//	Render the highchart
			createSparklines(widget, index, config, data)
		}
	}
]