//	Initialize the copyWidget object
prism.run([function() {

	//	Define the menu label
	var headerMenuCaption = "Sparkline",
		propName = 'sparkline';

	//	What chart types are supported?
	var supportedChartTypes = ["pivot"];

	//	default settings object	
	var defaultSettings = {};

	//////////////////////////////////
	//	Utility Functions			//
	//////////////////////////////////

	//	Function to determine if this chart type is supported
	function widgetIsSupported(type){
		if (supportedChartTypes.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	//  Function to make sure this is a settings menu for a measure
    function isSettingsMenu(args,location) {
        
        //  Try to evaluate
        try {

        	//	Must be in the widget editor
			var widgetEditorOpen = (prism.$ngscope.appstate === "widget");
			if (!widgetEditorOpen){
				return false
			}

            // Widget must be an allowed chart            
            if(!widgetIsSupported(prism.$ngscope.widget.type)){
                return false;
            }

            if (location == "metadataMenu") {

	            //	Make sure the user clicked on a metadata item menu
	            var isMetadataItemMenu = (args.settings.name === "widget-metadataitem");
	            if (!isMetadataItemMenu){
	            	return false;
	            }

	        } else if (location == "widgetMenu"){

	        	//	Make sure the user clicked on a widget menu
	            var isWidgetMenu = (args.ui.css == "w-menu-host");
	            if (!isWidgetMenu){
	            	return false;
	            }
	        }

        } catch(e) {
            return false;
        }

        return true;
    }

    //	Function to add an event handler
    function addHandler(widget,event,handler){
        if (!hasEventHandler(widget,event,handler)){
            widget.on(event, handler);
        }
    }

    //  Function to check if model contains certain event handler for specified event name
    function hasEventHandler(model, eventName, handler) {

        return model.$$eventHandlers(eventName).indexOf(handler) >= 0;
    }

	//	Event handler for when the metadata item menu was clicked
	function metadataItemClicked(){

    	//	Get a reference to the widget's settings
    	var currentSelection = this.item.jaql[propName],
    		caption = this.caption;

    	//	was this property selected before?
    	var existing = (currentSelection == caption);

    	//	Update the settings of the item
    	if (existing){
    		delete this.item.jaql[propName]
    	} else {
    		this.item.jaql[propName] = caption;
    	}

		//	Redraw the widget
		this.widget.refresh();
	}

    //	Function to add the widget menu item
    function addMenuItem(event, args){

		//	Check for metadata menu
		var metadataMenu = isSettingsMenu(args,'metadataMenu');
		if (metadataMenu) {
			addMetadataMenuItem(event, args);
			return;
		}
    }

    //	Function to add the metadata menu item
    function addMetadataMenuItem(event,args){

		//	Get the widget object
		var widget = event.currentScope.widget;

		//	Get the metadata item that was clicked
		var item = args.settings.item,
			panelItems = args.settings.item.$$panel.items;

		//  Create a separator
        var separator = {
            type: "separator"
        };

		//	Create settings menu
		var options = {
			caption: headerMenuCaption,
			items:[]
		};

		//	Loop through each available function
		chartTypes.forEach(function(func){

			//	Make sure the function object is valid
			var isValid = (func.label && func.run);
			if (isValid){

				//	Look for existing settings
				var currentSelection = $$get(item, 'jaql.' + propName, null);

				//	Create the base menu item
				var subItem = {
					caption: func.label,
					checked: currentSelection === func.label,
					closing: false,
					disabled: false,
					size: "xl",
					type: "check",
					widget : widget,
					functionObj: func,
					item: item,
					panelItems: panelItems,
					execute: metadataItemClicked
				}

				//	Add header item
				options.items.push(subItem);
			}
		})
		
		//	Add options to the menu
		args.settings.items.push(separator);
		args.settings.items.push(options);	
    }

    //	Function to check to see if an item has the property set
    function itemCheck(item, fields){

    	//	Check to see if the property is set
		var functionName = $$get(item, 'jaql.' + propName, false);

		//	Figure out which finance function needs to be run
		var myFunctionProp = chartTypes.filter(function(func){
			return func.label === functionName;
		});
		var myFunction = $$get(myFunctionProp,'0.run', null);

		//	Figure out the index for this item
		var indexes = fields.filter(function(resultItem){
			return resultItem.id == $$get(item, 'field.id', null);
		})
		var index = $$get(indexes, '0.index', -1);

		//	Return an object w/ everything needed
		return {
			'index': index,
			'run': myFunction,
			'name': functionName,
			'isValid': (functionName && myFunction && (index >= 0))
		}
    }

    //	Function to determine the unique key per row
	function getKey(queryStats, row){

		//	Init the key string
		var key = '';

		//	Loop through each dimension, and append to the key
		for (var j=0; j<queryStats.dimensions.length; j++){
			key += ('-' + $$get(row, j + '.text', ''));
		}

		return key;
	}

	//	Function to get the query stats (differs depending on event)
	function getQueryStats(widget, args, event){

		var queryStats;

		//	For query end event
		if(event == 'queryend'){

			//	Use the $jaql service to analyze the query
			queryStats = prism.$jaql.analyze(args.query.metadata);

			//	Also save a reference to the result's field properties
			queryStats.rawResult = args.rawResult;

			//	Store the stats in the widget object
			widget.options.queryStats = queryStats;	

		} else if ('ready') {

			//	Already been calculated, just grab the saved stats
			queryStats = widget.options.queryStats;
		}

		return queryStats;
	}

	//	Function to calculate the color for a single data point
	function getColor(value, colorFormatting, dataRow, conditionalMetadata, valueColumn){

		//	get a default color from the palette
		var defaultColor = prism.$ngscope.dashboard.style.options.palette.colors[0];

		//	Evaluate color formatting options
		if (colorFormatting.type == "color") {

			//	Single color for the every point in the chart
			return colorFormatting.color !== 'transparent' ? colorFormatting.color : defaultColor;

		} else if (colorFormatting.type == "condition") {


			//	Conditional formatting, need to loop through conditions
			for (var j=0; j<colorFormatting.conditions.length; j++) {

				//	Get the current condition
				var condition = colorFormatting.conditions[j];

				//	Get the value of the expression
				var compareValue;
				if (typeof condition.expression == "string") {
					//	Static Condition
					compareValue = parseFloat(condition.expression);
				} else {
					//	Calculated Condition, figure out which measure has the comparison value
					var compareIndexes = conditionalMetadata.filter(function(f){ 
						return f.jaql.formula === condition.expression.jaql.formula; 
					})
					compareIndex = $$get(compareIndexes,'0.index',-1);

					//	Look for the comparison value
					compareValue = $$get(dataRow, (valueColumn + 1 + compareIndex) + '.data', 0);
				}

				//	Evaluate the expression vs the value
				var check = false;
		        if (condition.operator == ">") {
	                check = (value > compareValue);
		        } else if (condition.operator == "<") {
	                check = (value < compareValue);
		        } else if (condition.operator == "=") {
	                check = (value == compareValue);
		        } else if (condition.operator == "≥") {
	                check = (value >= compareValue);
		        } else if (condition.operator == "≤") {
	                check = (value <= compareValue);
		        } else if (condition.operator == "≠") {
	                check = (value != compareValue);
		        }

		        //	If this condition was matched return the color, otherwise keep looping
		        if (check){
		        	return condition.color;
		        }	    
			}

			//	No colors matched (would've been returned earlier)
			return defaultColor;

		} else {

			//	Other case?
			return defaultColor;
		}
	}

	//	Recursive function to find a row's dim
	function findClosestDim(rows, rowNumber, fidx){

		var row = rows[rowNumber];
		var cell = $('.p-dim-member[fidx=' + fidx + ']', row);

		if (cell.length == 1) {
			return cell.text()
		} else if (rowNumber >= 0) {
			return findClosestDim(rows, rowNumber - 1, fidx)
		} else {
			return '';
		}
	}

	//	Function to build a formatting mask, depending on a level
	function getDateMask(level){

		if (level == 'years') {
			return 'yyyy';
		} else if (level == 'quarters') {
			return 'Q yyyy';
		} else if (level == 'months') {
			return 'MMM yyyy';
		}  else if (level == 'weeks') {
			return 'MMM d, yyyy';
		} else if (level == 'days') {
			return 'MMM d, yyyy';
		} else {
			return null;
		}
	}


	//////////////////////////////////
	//	Business Logic				//
	//////////////////////////////////

	//	Function to create a new JAQL Query
	function createJaql(queryStats, datasource, item){

		//	Create a new query
		var newQuery = {
			'datasource': datasource,
			'metadata': []
		}

		//	Add the dimensions 
		queryStats.dimensions.forEach(function(item){
			newQuery.metadata.push(item);
		})

		//	Break the finance function's jaql into more dimensions and measures
		var subDimensions = [],
			subMeasures = [],
			contexts = item.jaql.formula.match(/\[.*?\]/g);

		//	Loop through each part of the formula
		contexts.forEach(function(id){

			//	Make sure the context exists
			if (item.jaql.context.hasOwnProperty(id)){

				//	Create a jaql metadata item from the context
				var context = {
					jaql: item.jaql.context[id]
				};

				if (context.jaql.agg || context.jaql.formula){

					//	This item is a measure, so make sure to include the formatting (for colors)
					context.format = item.format;

					//	Save to the submeasures array
					subMeasures.push(context);
				} else {

					//	This item is a dimension, just save it as is
					subDimensions.push(context);
				}
			}
		})

		//	Add the sub dimensions
		subDimensions.forEach(function(item){
			newQuery.metadata.push(item);
		})				

		//	Add the sub measures
		subMeasures.forEach(function(item){
			newQuery.metadata.push(item);
		})	

		//	Check for conditional coloring, and add the conditions to the query
		var conditions = $$get(item, 'format.color.conditions', []);
		conditions.forEach(function(condition, i){

			//	Define the base metadata item, with additional attributes
			var colorJaql = {
				'highlevel': true,
				'operator': condition.operator,
				'color': condition.color,
				'index': i,
				'jaql': {}
			}

			//	What kind of condition is it?
			if (typeof condition.expression !== 'string') {

				//	calculated condition
				colorJaql.jaql = condition.expression.jaql
				
			} else {

				//return

				//	static condition
				colorJaql.jaql = {
					'type': 'measure',
					'formula': condition.expression
				}
			}

			//	Append to query
			newQuery.metadata.push(colorJaql);
		})	

		//	Add the dashboard/widget filters 
		queryStats.filters.forEach(function(item){
			newQuery.metadata.push(item);
		})

		return newQuery;
	}

	//	Function to update the resultset of pivot's specifically
	function cleanupTable(result, index, queryStats){

		//	Create an array to hold the color values
		var dims = [];

		//	Get a reference to the HTML table in memory
		var table = $('<div>' + result.table + '</div>');

		//	Loop through each row (skipping the first row of headers)
		var rows = $('tr', table);
		for (var j=1; j<rows.length; j++){

			//	Find the cell to convert to sparkline, and update
			var valueCell = $('td.p-value[fidx=' + index + ']', rows[j]);
			valueCell.text('');
			valueCell.removeAttr("style");
			valueCell.css('overflow','visible');
			valueCell.removeClass("colorable");

			//	Figure out the dimensionality of this row
			var members = [],
				key = '';
			queryStats.dimensions.forEach(function(dim, index){

				//	Look for the dimension cell
				var dimCell = findClosestDim(rows, j, index);
				members.push(dimCell);
				key += '-' + dimCell;
			})

			dims.push({
				members: members,
				key: key,
				rowNumber: j-1
			})
		}
		
		//	Return the html table, and the lookup for determining each row
		return  {
			table: table.html(),
			rows: dims
		}
	}

	//	Function to get the midpoint
	function cleanData(widget, data, queryStats, colorFormatting, dataMask) {

		//	Create a dictionary of the results, grouped by dimension member
		var newValues = {},
			results = [],
			rowNumber = 0,
			dimColumn = queryStats.dimensions.length,
			valueColumn = dimColumn + 1;

		//	Check for any conditional formatting fields
		var conditionalMetadata = data.metadata.filter(function(f) { 
			return f.highlevel 
		});

		//	Look for the level of a potential date dimension
		var dateLevel = $$get(data, 'metadata.' + dimColumn + '.jaql.level', null);
		var dateMask = getDateMask(dateLevel);

		//	Loop through every row of data, and group it based on the each row in the pivot
		data.values.forEach(function(row){

			//	Figure out the unique identified for this row
			var key = getKey(queryStats, row);
			if (key){

				//	Figure out what row this is
				var matches = queryStats.rowDictionary.filter(function(row){ 
					return row.key === key; 
				})
				var thisRowNumber = $$get(matches, '0.rowNumber', -1);

				//	Create an entry, if it doesn't exist
				if (!newValues[key]) {

					//	Create a new dictionaty entry for this key
					newValues[key] = {
						index: thisRowNumber,
						data: [],
						values: []
					}
					rowNumber += 1;
				}

				//	Get the raw data value
				var value = row[valueColumn].data;
				var valueText = formatter('numeric')(value, dataMask).replace("-",'‑');

				//	Calculate the text of the dimension
				var dimension = row[dimColumn].data;
				var dimensionText = dateLevel ? formatter('date')(dimension, dateMask) : dimension;

				//	Create a data point
				var point ={
					"rowMembers": $$get(matches, '0.members', []).join(', '),
					"dimension": dimensionText,
					"color": getColor(value, colorFormatting, row, conditionalMetadata, valueColumn),
					"y": value,
					"value": valueText
				};

				newValues[key].data.push(point);
				newValues[key].values.push(value);
			}
		})

		//	Put the values into a data frame
		for (key in newValues){

			//	Get the row object
			var row = newValues[key];

			//	Save to array, at the appropriate index
			results[row.index] = row.data;
		}

		return results;
	}

	//	Function to remove the dummy values from the initial pivot display
	function updatePivot(widget, args, event){

		//	Analyze the JAQL to be run
		var queryStats = getQueryStats(widget, args, event);

		//	Loop through each metadata item from the query
		queryStats.measures.forEach(function(item){

			//	Check to see if this measure is colored conditionally
			var colorIsConditional = $$get(item, 'format.color.type') === "condition";

			//	set a default color from either the color palette, or grey for conditional
			var defaultColor = colorIsConditional ? '#EDEEF1' : prism.$ngscope.dashboard.style.options.palette.colors[0];

			//	Safely get a formatting definition object
			var colorFormatting = $$get(item, 'format.color', { 'type': 'color', 'color': defaultColor });

			//	Get formatting mask
			var formatMask = $$get(item, 'format.mask', {});

			//	Figure out all things required for the function 
			var myFunction = itemCheck(item, queryStats.rawResult.fields);

			//	Do we have everything needed to run a function?
			if (myFunction.isValid){

				if (event == 'queryend') {

					//	Replace the dummy values with zeros
					var processedResult = cleanupTable(queryStats.rawResult, myFunction.index, queryStats);
					args.rawResult.table = processedResult.table;
					queryStats.rowDictionary = processedResult.rows;
					
				} else if (event == 'ready') {

					//	Remove any last minute coloring to the cells
					setTimeout(function(){

						//	Find any cells in this column
						var container = (prism.$ngscope.appstate === "widget") ? $('pivot') : $('widget[widgetid="' + widget.oid + '"]'),
							cells = $('td[fidx=' + myFunction.index + ']', container);

						//	Clear any background coloring of the cells
						cells.css('background-color','')
					},0)

					//	Create a new jaql query, to build the sparkline
					var newQuery = createJaql(queryStats,widget.datasource, item);

					//	Define the API call's options
					var options = {
						'method': 'POST',
						'url': '/jaql/query',
						'contentType': 'application/json',
						'data': JSON.stringify(newQuery),
					}

					//	Run the API call for this query
					$.ajax(options).done(function(response){

						//	Create a cleaned up data set from the query response
						var newData = cleanData(widget, response, queryStats, colorFormatting, formatMask);

						//	Create formatting description
						var formatting = {
							mask: formatMask,
							color: {
								color: colorFormatting.type,
								default: $$get( colorFormatting, 'color', defaultColor)
							}
						}

						//	Run the render function for the selected chart type
						myFunction.run(widget, myFunction.index, newData, formatting);
					})
				}
			}
		})
	}


	//////////////////////////////////
	//	Event Handlers				//
	//////////////////////////////////

	//	Event handler functions
	function queryended(widget,args){
		updatePivot(widget, args, 'queryend');
	}
	function widgetready(widget,args){
		updatePivot(widget, args, 'ready');
	}
	
	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
	});

	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
		//	Hooking to ready/destroyed events
		addHandler(args.widget, 'destroyed', onWidgetDestroyed);
		//	Should we run this?
		var shouldInit = widgetIsSupported(args.widget.type);
		//	Add hook for rendering
		if (shouldInit) {
			addHandler(args.widget, 'queryend', queryended);
			addHandler(args.widget, 'ready', widgetready);
		}
	}

	// unregistering widget events
	function onWidgetDestroyed(widget, args) {
		widget.off("destroyed", onWidgetDestroyed);
	}

	//	Create Options for the quadrant analysis
	prism.on("beforemenu", addMenuItem);

}]);