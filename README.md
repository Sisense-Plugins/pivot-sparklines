#	Sparklines in Pivot Tables

This plugin was created to allow dashboard designers to create sparklines within a pivot table.  This plugin currently supports line, area, and column charts as the sparklines.

![alt Dashboard](screenshots/dashboard.png)

__Step 1: Copy the plugin__ - Copy the pivotsparklines folder into your C:\program files\sisense\PrismWeb\plugins directory.

__Step 2: Create the pivot formula__ - The main challenge with sparklines is that they require more data than just what's displayed in the pivot table.  In order to specify the extra dimensions needed, you can create a multi-pass aggregation function, that this plugin will use to generate a new query.  The screenshots below show how to create a formula that includes the extra dimension needed to get supporting data used by the sparkline.  Please note, you can use either a measure or starred formula for the last item in the sparkline formula

![alt Formula Editor view](screenshots/formula.png)

__Step 3: Define your Sparkline__ -
Click on the settings menu of your formula, and then the sparkline option.  You should see a menu listing the available chart types.  

![alt Widget Editor view](screenshots/widget-editor.png)

If you change your formula, you must recheck the sparkline box.  Also, you can set the color of the sparkline, similarly to how you would for any other value in the pivot table.

__Notes/Reference__

* This sample has been confirmed working on Sisense version 7.1, and should be backwards compatible with previous version
* This plugin does not support exporting to PDF, XLS, or CSV
* This plugin leverages highcharts to render the sparklines, you can customize the look/behavior of the sparklines by editing the chartTypes object within _charts.js_